from django.forms import ModelForm
from recipes.models import Recipe
from django import forms

# Create a form class to link to a model class


class RecipeForm(ModelForm):
    class Meta:
        model = Recipe
        fields = [
            "title",
            "picture",
            "description",
            "gluten_free",
        ]
